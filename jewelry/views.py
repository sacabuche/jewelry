# coding=utf-8

from flask import render_template, request, redirect, url_for, flash
from flaskext.login import (login_required, login_user, logout_user,
                            confirm_login, current_user)

from jewelry import app, db
from models import Payment, Sell, Investor, SellerAgent, User
from forms import SellerAgentForm, SellForm, PaymentForm, LoginForm
from sqlalchemy import desc


@app.route('/seller_agents/')
@login_required
def show_seller_agents():
    is_active = SellerAgent.STATUS.active
    seller_agents = SellerAgent.query.filter_by(status=is_active)

    return render_template('seller_agents.html',
                           seller_agents = seller_agents)


@app.route('/seller-agent/<id>/edit/', methods=('GET','POST'))
@app.route('/seller-agent/add/', methods=('GET', 'POST'), defaults={'id':None})
@login_required
def seller_agent_edit_create(id):
    if id:
        seller = SellerAgent.query.filter_by(id=id).first_or_404()
    else:
        seller = SellerAgent()

    form = SellerAgentForm(request.form, seller)
    if form.validate_on_submit():
        form.populate_obj(seller)
        db.session.add(seller)
        db.session.commit()
        return redirect(seller.url)
    return render_template('seller_agent_edit_create.html', form=form,
                           seller=seller)

@app.route('/seller-agent/<id>/')
@login_required
def seller_agent(id):
    seller = SellerAgent.query.filter_by(id=id).first_or_404()
    sells = seller.sells.order_by(desc(Sell.date)).all()
    payments = seller.payments.order_by(desc(Payment.date)).all()
    return render_template('seller_agent.html',
                           seller=seller,
                           sells=sells,
                           payments=payments)

@app.route('/seller-agent/<agent_id>/sells/add/', methods=('GET','POST'))
@app.route('/seller-agent/<agent_id>/sells/<id>/', methods=('GET','POST'))
@login_required
def sells(agent_id, id=None):
    seller = SellerAgent.query.filter_by(id=agent_id).first_or_404()

    if id:
        sell = seller.sells.filter_by(id=id).first_or_404()
    else:
        sell = Sell(seller_agent=seller)

    # delete
    if request.form.get('delete', None):
        db.session.delete(sell)
        db.session.commit()
        return redirect(seller.url)

    # Add / Edit
    form = SellForm(request.form, sell)
    if form.validate_on_submit():
        if request.method == 'POST':
            form.populate_obj(sell)
            db.session.add(sell)
            db.session.commit()
        return redirect(seller.url)
    return render_template('sell.html',
                           form=form,
                           seller=seller,
                           sell=sell)

@app.route('/seller-agent/<agent_id>/payments/add/', methods=('GET','POST'))
@app.route('/seller-agent/<agent_id>/payments/<id>/', methods=('GET','POST'))
@login_required
def payments(agent_id, id=None):
    seller = SellerAgent.query.filter_by(id=agent_id).first_or_404()

    if id:
        payment = seller.payments.filter_by(id=id).first_or_404()
        # payment = Payment.query.filter_by(id=id).first_or_404()
    else:
        payment = Payment(seller_agent=seller)

    if request.form.get('delete', None):
        db.session.delete(payment)
        db.session.commit()
        return redirect(seller.url)

    form = PaymentForm(request.form, payment)
    if form.validate_on_submit():
        form.populate_obj(payment)
        db.session.add(payment)
        db.session.commit()
        return redirect(seller.url)
    return render_template('payment.html',
                           form=form,
                           seller=seller,
                           payment=payment)

@app.route("/", methods=["GET", "POST"])
@app.route("/login/", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        user = User.query.filter_by(username=username).first()
        if user and user.check_password(password) and login_user(user):
            flash("Bienvenido %s"%user.username)
            return redirect(request.args.get("next") or url_for('show_seller_agents'))
        flash(u'Usuario y/o Contraseña invalida')
    return render_template('login.html', form=form)


@app.route("/logout/")
@login_required
def logout():
    flash("Adios %s"%current_user.username)
    logout_user()
    return redirect(url_for('login'))
