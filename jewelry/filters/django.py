import re
from jinja2 import evalcontextfilter, Markup, escape
from jewelry import app


@app.template_filter()
@evalcontextfilter
def nl2br(eval_ctx, value):
    result = u'<br/>'.join(line
                          for line in escape(value).split('\n'))
    if eval_ctx.autoescape:
        result = Markup(result)
    return result
