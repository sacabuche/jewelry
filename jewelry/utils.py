import random
import string


class Options(dict):
    """
    Helper to find the options

    Example:
    >>> STATUS = Options({'active':0, 'inactive':2})
    >>> STATUS.active
    0
    >>> STATUS[0]
    'active'
    >>> STATUS['active']
    0
    """

    def __init__(self, *args, **kwargs):
        super(Options, self).__init__(*args, **kwargs)
        reversed_status = dict(((value, key) for key, value in self.items()))
        self.update(reversed_status)

    def __getattr__(self, name):

        try:
            return getattr(super(Options, self), name)
        except AttributeError:
            pass

        try:
            return self[name]
        except KeyError:
            raise AttributeError


def random_string(length):
    pool = string.letters + string.digits
    return ''.join(random.choice(pool) for i in xrange(length))

def pre_hash(salt, password):
    return password + salt
