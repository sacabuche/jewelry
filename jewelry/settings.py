import sqlite3
from flask import (Flask, request, session, g, redirect, url_for,
                   abort, render_template, flash)
import os

PROJECT_DIR = os.path.dirname(__file__)
BASE_DIR =  os.path.realpath(PROJECT_DIR)

SQLALCHEMY_DATABASE_URI = 'sqlite:///'+ BASE_DIR +'/jewelry.db'
DATABASE = SQLALCHEMY_DATABASE_URI
DEBUG = True
SECRET_KEY = "N9Tw9O86aJYlHAw9MlKGolD4Xq1shPPlMyrOIZv50N5RwCycUc"
SALT_LENGTH = 20


REMUNERATION_PERCENT = (
    # (Sell.quantity, remuneration_percent)
    # From 0 is 20%
    (0, 20),
    # From 2000 is 30%
    (2000, 30),
)
try:
    from local_settings import *
except ImportError:
    pass
