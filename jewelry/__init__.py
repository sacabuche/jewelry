from flask import Flask
from flaskext.sqlalchemy import SQLAlchemy
from flaskext.login import LoginManager
from flaskext.bcrypt import Bcrypt


import settings

app = Flask(__name__)
app.config.from_object(settings)

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)

login_manager = LoginManager()
login_manager.login_view = "/login/"


login_manager.setup_app(app)


from models import User

@login_manager.user_loader
def load_user(userid):
    return User.query.get(userid)


import filters
import views
