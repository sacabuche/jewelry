from flask import url_for
from jewelry import db, app, bcrypt
from jewelry.utils import Options, random_string, pre_hash
from flaskext.login import UserMixin

from sqlalchemy import event, desc
from sqlalchemy.orm import validates

from decimal import *


class PersonMixin(object):

    STATUS = Options(active = 0,
                     inactive = 1,
                     deleted = 2)

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400), nullable=False)
    email = db.Column(db.String(400), default='')
    tel = db.Column(db.String(40), default='')
    address = db.Column(db.Text, default='')
    status = db.Column(db.SmallInteger, default=STATUS.active)

class OperationMixin(object):
    id = db.Column(db.Integer, primary_key=True)
    quantity =  db.Column(db.Numeric(precision=10, scale=3))
    date = db.Column(db.Date, default=db.func.now())


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)
    password_hash = db.Column(db.String(200), nullable=False)
    salt = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(200), nullable=False)
    is_active = db.Column(db.Boolean(), default=True)

    def _pre_hash(self, salt, password):
        return salt + password

    def set_password(self, password):
        self.salt = random_string(app.config.get('SALT_LENGTH', 180))
        _pre_hash = pre_hash(self.salt, password)
        self.password_hash = bcrypt.generate_password_hash(_pre_hash)

    def check_password(self, password):
        _pre_hash = pre_hash(self.salt, password)
        return bcrypt.check_password_hash(self.password_hash, _pre_hash)

    def is_active(self):
        return self.is_active

    def is_anonymous(self):
        return False

    @classmethod
    def new(cls, username, email, password):
        instance = cls(username=username, email=email)
        instance.set_password(password)
        db.session.add(instance)
        db.session.commit()
        return instance


class SellerAgent(PersonMixin, db.Model):

    __tablename__ = 'seller_agents'

    #debt = db.Column(db.Numeric(precision=10, scale=3), default=Decimal(0),
    #                nullable=False)
    #last_payment = db.Column(db.Date)

    @property
    def debt(self):
        to_pay = sum(sell.to_pay for sell in self.sells.all())
        payed = sum(payment.quantity for payment in self.payments.all())
        return to_pay - payed

    @property
    def url(self):
        return url_for('seller_agent', id=self.id)

    @property
    def url_add_sell(self):
        return url_for('sells', agent_id=self.id)

    @property
    def url_add_payment(self):
        return url_for('payments', agent_id=self.id)

    @property
    def url_edit(self):
        return url_for('seller_agent_edit_create', id=self.id)

    @property
    def last_payment(self):
        return self.payments.order_by(desc(Payment.date)).first()


class Investor(PersonMixin, db.Model):
    __tablename__ = 'investors'


class Sell(OperationMixin, db.Model):
    __tablename__ = 'sells'

    id = db.Column(db.Integer, primary_key=True)
    seller_agent_id = db.Column(db.Integer, db.ForeignKey('seller_agents.id'),
                               nullable=False)
    remuneration_percent = db.Column(db.Float)
    remuneration = db.Column(db.Numeric(precision=10, scale=3), nullable=False)
    to_pay = db.Column(db.Numeric(precision=10, scale=3), nullable=False)
    seller_agent = db.relationship('SellerAgent',
                                  backref=db.backref('sells',
                                                     lazy='dynamic'))
    @property
    def url(self):
        return url_for('sells', agent_id=self.seller_agent_id, id=self.id)

    #@validates('remuneration_percent')
    #def validate_remuneration_percent(self, name, value):
    #    if (value <= 100) and (value >= 0):
    #        return value
    #    raise ValueError



class Payment(OperationMixin, db.Model):
    __tablename__ = 'payments'

    id = db.Column(db.Integer, primary_key=True)
    seller_agent_id = db.Column(db.Integer, db.ForeignKey('seller_agents.id'))
    seller_agent = db.relationship('SellerAgent',
                                  backref=db.backref('payments',
                                                     lazy='dynamic'))
    @property
    def url(self):
        return url_for('payments', agent_id=self.seller_agent_id, id=self.id)


@event.listens_for(Sell.__mapper__, 'before_update')
@event.listens_for(Sell.__mapper__, 'before_insert')
def sell_before_insert(mapper, connection, target):
    """
    Calculates and set remuneration and to_pay
    """
    remuneration_percent = target.remuneration_percent
    quantity = target.quantity

    # Validate quantity superior to 0
    if quantity < 0:
        raise ValueError('Quantity must be a positive number')

    if not remuneration_percent:
        R_PERCENT = app.config['REMUNERATION_PERCENT']
        percents = [p for q, p in R_PERCENT if quantity > q]
        # Get last percent
        target.remuneration_percent = percents.pop()
        remuneration_percent = target.remuneration_percent

    if (remuneration_percent < 0) or (remuneration_percent > 100):
        raise ValueError('Remuneration Percent must be between 0-100 not:%s'\
                         %remuneration_percent)

    percent = target.remuneration_percent/Decimal(100)
    # calculate needed values
    target.remuneration = percent * target.quantity
    target.to_pay = target.quantity - target.remuneration


#@event.listens_for(db.session(), "after_flush")
#def after_flush(session, flush_context):
#    for obj in session.new:
#        # Sell
#        if isinstance(obj, Sell):
#            obj.seller_agent.recalculate_debt()
#            session.add(obj.seller_agent)
#        # Payment
#        elif isinstance(obj, Payment):
#            obj.seller_agent.recalculate_debt()
#            obj.seller_agent.last_payment = obj.date
#            session.add(obj.seller_agent)
#    print session.new
#    print session.dirty
#    #session.flush()



"""
from jewelry import db
from jewelry.models import Sell, SellerAgent
from decimal import *

seller = SellerAgent.query.first()
#sell = Sell(quantity=Decimal(1200.12), seller_agent=seller,
#remuneration_percent=30)

sell = Sell(quantity=Decimal(1200.12), seller_agent=seller)
db.session.add(sell)
db.session.commit()
"""
