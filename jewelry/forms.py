# coding=utf-8

from wtforms.ext.sqlalchemy.orm import model_form
from flaskext.wtf import Form, TextField, Required, PasswordField

from jewelry.models import SellerAgent, Sell, Payment

SellerAgentForm = model_form(SellerAgent, Form)
SellForm = model_form(Sell, Form, field_args={'remuneration_percent':
                                                    {'validators':[]}
                                             })
PaymentForm = model_form(Payment, Form)

class LoginForm(Form):
    username = TextField('usuario', validators=[Required()])
    password = PasswordField(u'contraseña', validators=[Required()])
